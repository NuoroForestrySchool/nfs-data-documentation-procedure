---
header-includes:
- \usepackage{fancyhdr}
- \usepackage{lastpage}
- \pagestyle{fancy}
- \lfoot{\textit{NuoroForestrySchool-FAIR data documentation procedure}}
- \cfoot{}
- \rfoot{pag. \thepage\, /\, \pageref*{LastPage}}
output:
  pdf_document:
    df_print: kable
    extra_dependencies: float
documentclass: article
classoption: a4paper
editor_options:
  chunk_output_type: console
date: '`r format(Sys.Date(), format = "%d %B %Y")`'
title: Experimenal evaluation, in a four years old coppice stand, of water runoff
  and soil erosion with and without shoots canopy coverage, under intense artificial
  rainfall
author:
- 'Giadrossich, Filippo^[Orcid: 0000-0002-7546-1632]'
- '<to be added> ...^[Orcid: 0000-0000-0000-0000]'
- 'Scotti, Roberto^[Orcid: 0000-0001-7394-4473]'
keywords:
- Coppice forest
- ' Forest management'
- ' Quercus ilex'
- ' Silviculture'
- ' soil protection'
- ' Sardinia (Italy)'
- ' forest soil properties'
abstract: "This paper documents the structure and contents of the \"2018_SoilErosionTrials_Marganai_Sardegna\"\n
  database. The database and this document are accessible through 'DOI: to be updated!!
  doi.org/10.1594/PANGAEA.943451'\nThe database and this document are produced using
  the \"NuoroForestrySchool\n - FAIR - Data Documentation Procedure\". The project
  is accessible through the GitLab\n repository (https://gitlab.com/NuoroForestrySchool/nfs-data-documentation-procedure).
  \   \n _________________    \n Database keywords: Coppice forest, Forest management,
  Quercus ilex, Silviculture, soil protection, Sardinia (Italy), forest soil properties"
---
```{r setup, echo = FALSE}
purrr::walk(c("tidyverse", "fs", "DBI", "kableExtra", "knitr"), 
    function(p) suppressPackageStartupMessages(library(p, 
        character.only = TRUE)))
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(fig.pos = "H", out.extra = "")
opts <- options(knitr.kable.NA = "")
my_kable <- function(df, ...) {
    df %>% kable(..., booktabs = TRUE, longtable = T, align = "l") %>% 
        kable_styling(latex_options = c("repeat_header", 
            "striped", "HOLD_position"))
}
dbConnectIfExists <- function(sqlite_file) {
    if (file.exists(sqlite_file)) 
        return(dbConnect(RSQLite::SQLite(), sqlite_file))
    else {
        cat(" **** >>> NO such FILE: ", sqlite_file)
        return(NULL)
    }
}
dbMakeTibble <- function(tb, db, verbose = FALSE) {
    if (!dbIsValid(db)) {
        stop(paste(" *** >>> Invalid db connection:", db))
        return(FALSE)
    }
    if (!tb %in% dbListTables(db)) {
        cat(" *** >>> Table:", tb, "/n         not found in file: ", 
            basename(db@dbname), "/n         in folder: ", 
            dirname(db@dbname))
        return(FALSE)
    }
    as_tibble(assign(tb, envir = .GlobalEnv, (suppressMessages(dbReadTable(db, 
        tb)))))
    if (verbose) 
        cat("  *** Tibble: '", tb, "' created!/n")
    return(invisible(TRUE))
}
DBfilePath <- structure("OUTPUT/2018_SoilErosionTrials_Marganai_Sardegna.sqlite", class = c("fs_path", 
"character")) %>% path_file()
db <- dbConnectIfExists(DBfilePath)
if (is.null(db)) 
    stop(" >>>>> in DBconnection!")
```
```{r TheExperiment}
dbReadTable(db, "meta.References") %>% filter(str_detect(key, 
    "ExperimentSynthesis")) %>% select(value) %>% rename(`Experiment synthesis` = value) %>% 
    my_kable() %>% column_spec(1, width = "12cm")
```
\bigskip \textbf{Experiment description}   \newline ---------- \newline  Marganai forest, after some decades of interruption, the once customary coppicing practices were reactivated, in the year 2015, implementation of a new forest management plan (D.R.E.Am. Italia & R.D.M. Progetti, 2014). For such practices the administration has been accused of promoting soil erosion (Branca et. Al, 2020). In 2018 Regione Sardegna financed (within Progetto Sulcis, 2018) this experimental project in the attempt to face the problem by adopting a scientific approach. To evaluate the effect of forest cover on sediment transport due to rain, following trials have been set up. A set of 8 test plots pairs (or blocks) have been localized In two neighboring hillslopes (forest management parcels 20 and 41, 4 blocks per part) with similar conditions regarding soil characteristics, tree species, stumps density and size. Each block included the contrasting situations: a plot with canopy cover (i.e. with a stump and its sprouts in the center of the plot) and one without a forest canopy cover. Soil characteristics (texture, skeleton, organic matter) have been tallied and its hydraulic conductivity has been measured both as saturation values and while experimental rain was delivered. Individual plots are 1 m wide and 1.5 m long, aligned along the maximum slope gradient. On the downslope end water is collected, while other sides are delimited by planks. At the edge of plots, 3 rainfall gauges were positioned to check the rainfall distribution during each test. So the actual simulated rainfall intensity was calculated by weighted average according to thiessen polygons. Artificial rain was provided, for two or three 30 min periods, with a mean intensity of 43 mm*h^-1, greatly exceeding rainfall records. The equipment consisted in a wooden tripod with a modular structure carrying the nozzle, similar to that used in Commandeur (1992), positioned 4 m above the ground, central with respect to the plot surface, in order to provide a homogeneous distribution of rainfall. The nozzle used is a calibrated Lechler nozzle (mod. 490.888) with a jet width of 120 degrees, to which an average water pressure of 1.5 bar was applied. Runoff and sediments were collected in bottles of 0.5 liters, renewed every 5 minutes. A total of 222 samples containing water and sediments were assembled. All samples were weighted in the field and stored for laboratory analysis, where the separate weight of soil (solid particles) and organic matter has been determined. Due to the scarcity of sediments in each bottle, analysis has been carried out using 30 minutes pooled samples.

References

Branca, G., Piredda, I., Scotti, R., Chessa, L., Murgia, I., Ganga, A., Campus. S.F., Lovreglio, L., Guastini, E., Schwarz, M., and Giadrossich, F. (2020). Forest protection unifies, silviculture divides: A sociological analysis of local stakeholders’ voices after coppicing in the Marganai forest (Sardinia, Italy). Forests, 11(6), 708.

Commandeur, P. R. (1992, July). Soil erosion studies using rainfall simulation on forest harvested areas in British Columbia. Proceedings of the international symposium on Erosion, Debris Flows and Environment in Mountain Regions. IAHS publication (No. 209, pp. 21-23).

D.R.E.Am. Italia & R.D.M. Progetti, 2014. Piano forestale particolareggiato del complesso forestale "Marganai" - RAS - EFS. Http://www.sardegnaambiente.it/documenti/3_68_20140701114331.pdf (last visited 22Feb2022)

Progetto Sulcis, 2018. Project funded by Regione Sardegna (Italy), co-funded by European Regional Development Fund (ROP Sardegna ERDF) [project code SULCIS-820965]. Project title: “Environmental and socio-economic sustainability of coppice forest utilization in Marganai”.
```{r ERD, echo = FALSE, out.height = "90%", fig.align = "left", fig.cap = "The database schema as ERD"}
ERDimageName <- "ERD_schema.png"
dbMakeTibble("DDict.schema", db)
DDict.schema %>% filter(Format == "ERD image") %>% select(File) %>% 
    pluck(1, 1) %>% writeBin(con = ERDimageName)
knitr::include_graphics(ERDimageName)
```
```{r TABLES}
tl <- 12
dbReadTable(db, "DDic.Tables") %>% filter(!is.na(caption)) %>% 
    mutate(table = strsplit(table, paste0("(?<=.{", tl, "})"), 
        perl = TRUE)) %>% my_kable(caption = "Database tables") %>% 
    column_spec(1, "2cm") %>% column_spec(2, "7cm") %>% column_spec(3, 
    "3cm")
```
```{r DDICT}
dbReadTable(db, "DDic.Attributes") %>% filter(!is.na(caption)) %>% 
    my_kable(caption = "Data dictionary") %>% column_spec(1, 
    width = "4cm") %>% column_spec(2, width = "1.3cm") %>% 
    column_spec(3, width = "1.3cm") %>% column_spec(4, width = "6cm")
```
\pagebreak \textbf{SQL listing of DataBase SCHEMA}
```{r SQL_schema}
DDict.schema %>% filter(Format == "Formatted SQL") %>% select(File) %>% 
    pluck(1, 1) %>% rawToChar() %>% cat()
```
