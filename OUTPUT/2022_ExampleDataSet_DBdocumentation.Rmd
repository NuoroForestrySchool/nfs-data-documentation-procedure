---
header-includes:
- \usepackage{fancyhdr}
- \usepackage{lastpage}
- \pagestyle{fancy}
- \lfoot{\textit{NuoroForestrySchool-FAIR data documentation procedure}}
- \cfoot{}
- \rfoot{pag. \thepage\, /\, \pageref*{LastPage}}
output:
  pdf_document:
    df_print: kable
    extra_dependencies: float
documentclass: article
classoption: a4paper
editor_options:
  chunk_output_type: console
date: '`r format(Sys.Date(), format = "%d %B %Y")`'
title: Expressive title
author:
- 'me^[Orcid: 0000-0000-0000-0000]'
- 'you^[Orcid: 0000-0000-0000-0000]'
keywords:
- my key word 1
- ' my key word 2'
- ' my key word 3'
abstract: "This paper documents the structure and contents of the \"OUTPUT/2022_ExampleDataSet.sqlite\"\n
  database. The database and this document are accessible through DOI: to_be_specified
  \ \nThe database and this document are produced using the \"NuoroForestrySchool\n
  - FAIR - Data Documentation Procedure\". The project is accessible through the GitLab\n
  repository (https://gitlab.com/NuoroForestrySchool/nfs-data-documentation-procedure).
  \   \n _________________    \n Database keywords: my key word 1, my key word 2,
  my key word 3"
---
```{r setup, echo = FALSE}
suppressPackageStartupMessages(library(tidyverse))
library(DBI)
library(knitr)
library(fs)
suppressPackageStartupMessages(library(kableExtra))
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(fig.pos = "H", out.extra = "")
opts <- options(knitr.kable.NA = "")
my_kable <- function(df, ...) {
    df %>% kable(..., booktabs = TRUE, align = "l") %>% kable_styling(latex_options = "HOLD_position")
}
DBfile <- structure("OUTPUT/2022_ExampleDataSet.sqlite", class = c("fs_path", 
"character")) %>% path_file()
db <- dbConnect(RSQLite::SQLite(), DBfile)
```
```{r TheExperiment}
dbReadTable(db, "meta.REFERENCES") %>% filter(str_detect(key, 
    "Experim")) %>% select(value) %>% pluck(1) %>% my_kable(col.names = NULL) %>% 
    kable_styling(full_width = TRUE)
```
```{r ERD, echo = FALSE, out.height = "75%", fig.align = "left", fig.cap = "The database ERD schema"}
knitr::include_graphics(structure("OUTPUT/ERD.png", class = c("fs_path", 
"character")) %>% path_file())
```
```{r TABLES}
dbReadTable(db, "meta.TABLES") %>% filter(!is.na(Caption)) %>% 
    my_kable(caption = "Database tables") %>% kable_styling(full_width = TRUE)
```
```{r DDICT}
dbReadTable(db, "meta.DDICT") %>% filter(!is.na(Caption)) %>% 
    my_kable(caption = "Data dictionary") %>% kable_styling(full_width = TRUE) %>% 
    column_spec(1, width = "4cm") %>% column_spec(2, width = "1.3cm") %>% 
    column_spec(3, width = "1.5cm") %>% column_spec(4, width = "6cm")
```
```{r SQL_schema}
read_file(structure("OUTPUT/schema.txt", class = c("fs_path", 
"character")) %>% path_file()) %>% cat()
```
