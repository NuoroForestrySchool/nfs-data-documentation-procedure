#!/bin/bash
if [ $# != 2 ]; then            # two args are required!
	echo "ERROR - Usage: $0 <SQLite_file_name> <ERD_file_name>"
	exit 1
fi
if ! [ -a "$1" ]; then          # it should correspond to an accessible file
# if ! test -a "$1"; then
	echo "ERROR - Usage: $0 <existing_file_name>  <ERD_file_name>"
	exit 1
fi
SQLite=$(head -c 16 < $1 | tr -d '\0' )
if [ "$SQLite" != "SQLite format 3" ]; then   # the file has to be an SQLite3 file
	echo "ERROR - '$1' is not an SQLite3 file ( '$SQLite' )"
	exit 1
fi
echo "---> chk pwd = $(pwd)"

DB=$1
ERD=$2
echo $ERD
if [ -a "$ERD" ]; then 
	rm $ERD
fi	
# 3 feb 2022
# https://dev.to/sualeh/how-to-visualize-your-sqlite-database-with-one-command-and-nothing-to-install-1f4m
docker run \
--mount type=bind,source="$(pwd)",target=/home/schcrwlr \
--rm=FALSE \
schemacrawler/schemacrawler \
/opt/schemacrawler/bin/schemacrawler.sh \
--server=sqlite \
--database=$DB \
--info-level=standard \
--command=schema \
--output-file=$ERD
